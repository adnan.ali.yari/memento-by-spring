package xor.anonymous_api.memento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xor.anonymous_api.memento.dao.MovieRatingRepository;
import xor.anonymous_api.memento.models.MovieRating;

import java.util.List;

@Service
public class MovieRatingService {

    @Autowired
    private MovieRatingRepository repository;


    public void putRating(MovieRating rating){ repository.save(rating); }

    public void postRating(MovieRating rating){
        repository.save(rating);
    }

    public MovieRating getRatingById(long id){ return repository.findById(id).get();}

    public List<MovieRating >getRatingByUserName(String name){ return repository
            .findAllByUserName(name);
        }

    public void deleteRating(long id){ repository.deleteById(id);}
}
