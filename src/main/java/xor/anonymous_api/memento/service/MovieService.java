package xor.anonymous_api.memento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xor.anonymous_api.memento.models.MovieRating;
import xor.anonymous_api.memento.dao.MovieRepository;
import xor.anonymous_api.memento.models.Movie;

import java.util.List;

@Service
public class MovieService {

    @Autowired
    private MovieRepository repository;

    public Movie getMovieById(long id){
        return repository.findById(id).get();
    }

    public Movie getMovieByName(String name){
        return repository.findMovieByName(name);
    }

    public List<Movie> getMoviesByNameContains(String regex){
        return repository.findMoviesByNameContains(regex);
    }

    public void putMovie(Movie movie, long id){
        if(repository.existsById(id)) {
            repository.findById(id);
        }
        postMovie(movie);
    }

    public void postMovie(Movie movie) {
        repository.save(movie);
    }

    public void updateMovieRating(MovieRating rating) {
        Movie movie = getMovieById(rating.getMovie().getId());
        movie.addRating(rating);
        repository.save(movie);
    }
}
