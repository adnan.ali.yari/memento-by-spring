package xor.anonymous_api.memento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import xor.anonymous_api.memento.dao.UserRepository;
import xor.anonymous_api.memento.models.User;
import xor.anonymous_api.memento.models.MovieRating;

@Service
public class UserService implements UserDetailsService {
    private UserRepository repository;

    @Autowired
    public void setRepository(UserRepository repository) {
        this.repository = repository;
    }

    public User getUserById(long id){
        return repository.findById(id).get();
    }

    public void putUserById(User alteredUser){
        postUser(alteredUser);
    }

    public void postUser(User newUser){
        repository.save(newUser);
    }

    public User getUserByName(String name) {
        return repository.findByName(name);
    }

    public User getUserByEmail(String email) {
        return repository.findByEmail(email);
    }

    public void addMovieRatingForUser(MovieRating rating) {
        User user = getUserById(rating.getUser().getId());
        user.addRating(rating);
        repository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String name)
            throws UsernameNotFoundException {
        User user = getUserByName(name);
        if (user != null) {
            return user;
        }
        throw new UsernameNotFoundException("User '" + name + "' not found");
    }
}
