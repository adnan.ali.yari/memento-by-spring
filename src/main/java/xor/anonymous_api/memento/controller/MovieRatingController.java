package xor.anonymous_api.memento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xor.anonymous_api.memento.models.MovieRating;
import xor.anonymous_api.memento.service.MovieRatingService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/MovieRating")
public class MovieRatingController {

    @Autowired
    private MovieRatingService service;

    @RequestMapping(value = "/id={id}", method = RequestMethod.GET)
    public MovieRating getMovieRatingById(@PathVariable("id") long id) throws CloneNotSupportedException {
        return (MovieRating) service.getRatingById(id).clone();
    }

    @RequestMapping(value = "/name={name}", method = RequestMethod.GET)
    public List<MovieRating> getAllMovieRatingsByName(@PathVariable("name") String name) {
        return service.getRatingByUserName(name).stream().map(movie -> {
            try {
                return (MovieRating) movie.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                return null;
            }
        }).collect(Collectors.toList());
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public void putMovieRating(@RequestBody MovieRating rating){ service.putRating(rating);}

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void postMovieRating(@RequestBody MovieRating rating){
        service.postRating(rating);
    }

    @RequestMapping(value = "/id={id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable("id") long id){
        service.deleteRating(id);
    }
}
