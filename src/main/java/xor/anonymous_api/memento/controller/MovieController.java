package xor.anonymous_api.memento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xor.anonymous_api.memento.contentProvider.HtmlContentProvider;
import xor.anonymous_api.memento.models.Movie;
import xor.anonymous_api.memento.models.MovieRating;
import xor.anonymous_api.memento.service.MovieService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    private MovieService service;

     @RequestMapping(value = "/", method = RequestMethod.GET)
     public String getMovieById(){
         return HtmlContentProvider.getMovieDefaultPageContent();
     }

    @RequestMapping(value = "/name={name}", method = RequestMethod.GET)
    public Movie getMovieByName(@PathVariable final String name) throws CloneNotSupportedException {
        return (Movie) service.getMovieByName(name).clone();
    }

    @RequestMapping(value = "/nameContains={name}", method = RequestMethod.GET)
    public List<Movie> getMovieByNameContains(@PathVariable final String name) {
        return service.getMoviesByNameContains(name).stream().map(movie -> {
            try {
                return (Movie) movie.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                return null;
            }
        }).collect(Collectors.toList());
    }

    @RequestMapping(value = "/id={id}", method = RequestMethod.PUT)
    public void putMovie(@RequestBody final Movie movie, @PathVariable final long id){
        service.postMovie(movie);
    }

    @RequestMapping(value = "/updateRatings", method = RequestMethod.PUT)
    public void updateMovieRatings(@RequestBody final MovieRating rating){
        service.updateMovieRating(rating);
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void postMovie(@RequestBody final Movie movie){
        service.postMovie(movie);
    }
}
