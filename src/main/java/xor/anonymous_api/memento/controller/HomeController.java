package xor.anonymous_api.memento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import xor.anonymous_api.memento.models.User;
import xor.anonymous_api.memento.service.UserService;

@Controller
public class HomeController {

    @Autowired
    UserService userService;


    @GetMapping(value = "/")
    public String home() {
        return "index.html";
    }
}
