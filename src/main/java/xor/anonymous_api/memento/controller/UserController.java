package xor.anonymous_api.memento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xor.anonymous_api.memento.contentProvider.HtmlContentProvider;
import xor.anonymous_api.memento.models.User;
import xor.anonymous_api.memento.service.UserService;
import xor.anonymous_api.memento.models.MovieRating;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String userWelcome(){
        return HtmlContentProvider.getUserDefaultPageContent();
    }

    @RequestMapping(value = "/name/", method = RequestMethod.GET)
    public User getUserByName(@RequestParam String name) throws CloneNotSupportedException {
        return (User) service.getUserByName(name).clone();
    }

    @RequestMapping(value = "/email/", method = RequestMethod.GET)
    public User getUserByEmail(@RequestParam String email) throws CloneNotSupportedException {
        return (User) service.getUserByEmail(email).clone();
    }

    @RequestMapping(value = "/name/", method = RequestMethod.PUT)
    public void putUser(@RequestBody User user, @RequestParam String name){
        service.putUserById(user);
    }

    @RequestMapping(value = "/updateRatings", method = RequestMethod.PUT)
    public void updateUserRatings(@RequestBody MovieRating rating){
        service.addMovieRatingForUser(rating);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void postUser(@RequestBody User user){
        service.postUser(user);
    }

}
