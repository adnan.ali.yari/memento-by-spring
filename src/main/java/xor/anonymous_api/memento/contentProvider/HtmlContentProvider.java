package xor.anonymous_api.memento.contentProvider;

public class HtmlContentProvider {

    public static String getMovieDefaultPageContent() {
        return "<P1>Welcome to Movie Page!<P1>";
    }
    public static String getUserDefaultPageContent() {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <title>MEMENTO</title>\n" +
                "</head>\n" +
                "<body bgcolor=\"#366\">\n" +
                "<P align=\"center\">Welcome to Project \"MEMENTO\"</P>\n" +
                "\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";
    }
}
