package xor.anonymous_api.memento.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import xor.anonymous_api.memento.models.Movie;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    List<Movie> findMoviesByNameContains(String name);
    Movie findMovieByName(String name);

    boolean existsByName(String name);
}
