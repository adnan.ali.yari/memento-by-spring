package xor.anonymous_api.memento.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xor.anonymous_api.memento.models.MovieRating;

import java.util.List;

@Repository
public interface MovieRatingRepository extends JpaRepository<MovieRating, Long> {
    List<MovieRating> findAllByMovieName(String name);
    List<MovieRating> findAllByUserName(String name);
    List<MovieRating> findAllByRatingGreaterThan(int x);

    boolean existsByUserName(String userName);
    boolean existsByUserEmail(String userEmail);
    boolean existsByMovieName(String movieName);
}
