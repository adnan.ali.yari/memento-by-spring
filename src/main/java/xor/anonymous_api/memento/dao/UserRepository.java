package xor.anonymous_api.memento.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import xor.anonymous_api.memento.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByName(String name);
    User findByEmail(String email);

    boolean existsByName(String name);
    boolean existsByEmail(String email);
}
