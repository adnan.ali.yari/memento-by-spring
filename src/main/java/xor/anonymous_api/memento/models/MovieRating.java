package xor.anonymous_api.memento.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class MovieRating {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private long id;
    @Column(name = "rating")
    private int rating;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id", referencedColumnName = "id")  
    private User user;

    @ManyToOne(targetEntity = Movie.class)
    @JoinColumn(name = "movie_id", referencedColumnName = "id")
    private Movie movie;

    public MovieRating() {
    }

    public MovieRating(int rating, User user, Movie movie) {
        this.rating = rating;
        this.user = user;
        this.movie = movie;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        MovieRating that = (MovieRating) o;
        return user.equals(that.user) && movie.equals(that.movie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, movie);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new MovieRating(rating, (User)user.clone(), (Movie)movie.clone());
    }

    @Override
    public String toString() {
        return "user "+user.getName()+" rated "+rating+" to movie "
            +movie.getName();
    }

}
