package xor.anonymous_api.memento.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Movie {

    @Column(name = "name")
    private String name;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private long id;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "movie", fetch = FetchType.LAZY)
    private Set<MovieRating> ratings = new HashSet<>();

    public Movie() {
    }

    public Movie(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<MovieRating> getRatings() {
        return ratings;
    }

    public void setRatings(Set<MovieRating> ratings) {
        this.ratings = ratings;
    }

    public void addRating(MovieRating movieRating) {
        ratings.add(movieRating);
    }

    public void removeRating(MovieRating movieRating) {
        ratings.remove(movieRating);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Movie movie = (Movie) o;
        return id == movie.id && name.equals(movie.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
	public Object clone() throws CloneNotSupportedException {
        return new Movie(name);
    }

    @Override
    public String toString() {
        return "movie "+name;
    }
}
