package xor.anonymous_api.memento.models;

import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

@Data
public class RegistrationForm {

    private String name;
    private String email;
    private String password;

    public User toUser(PasswordEncoder passwordEncoder) {
        return new User(
                name, passwordEncoder.encode(password), email, Arrays.asList("USER"));
    }
}
